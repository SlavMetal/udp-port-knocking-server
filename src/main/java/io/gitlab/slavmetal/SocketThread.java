/*******************************************************************************
 * Copyright (c) 2018 SlavMetal.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the accompanying LICENSE file.
 *******************************************************************************/
package io.gitlab.slavmetal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * UDP Socket to listen for knocks and create TCP server if needed.
 */
class SocketThread implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(SocketThread.class);
    private DatagramSocket udpServerSocket;                     // UDP server socket
    private List<String> portSequence;                          // Correct sequence of ports to open TCP connection
    private Map<String, ArrayList<Integer>> usersKnocksMap;     // Map to store current/correct knocks of users
    private int socket;                                         // Port number to listen UDP connections on

    private final static String KNOCK_MESSAGE = "Knock-knock";  // String which should be in the 'knock'

    /**
     * @param socket            UDP socket to open.
     * @param portSequence      correct sequence of knocks.
     * @param usersKnocksMap    map of user's knocks shared between threads.
     * @throws SocketException  if port can't be opened.
     */
    SocketThread(int socket, List<String> portSequence, Map<String, ArrayList<Integer>> usersKnocksMap) throws SocketException {
        this.portSequence = portSequence;
        this.usersKnocksMap = usersKnocksMap;
        this.socket = socket;

        udpServerSocket = new DatagramSocket(this.socket);
        logger.info("Opened UDP socket " + this.socket);
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                // Input buffer
                byte[] bufferInput = new byte[256];
                // Receive UDP packet
                DatagramPacket udpInput = new DatagramPacket(bufferInput, bufferInput.length);
                udpServerSocket.receive(udpInput);

                // Skip processing request if it's content is wrong
                if(!(new String(udpInput.getData(), 0, udpInput.getLength()).equals(KNOCK_MESSAGE)))
                    break;

                switch (addKnock(udpInput.getAddress().getHostAddress(), udpInput.getPort())){
                    case 1:
                        logger.info("Open TCP for the client");
                        // Initialize and start TCP server
                        TCPServer tmpTCPServer = new TCPServer(udpInput.getAddress().getHostAddress());
                        new Thread(tmpTCPServer).start();

                        // Send packet with it's port to the client
                        byte[] buffOut = String.valueOf(tmpTCPServer.getPort()).getBytes();
                        DatagramPacket udpOutput = new DatagramPacket(buffOut, buffOut.length, udpInput.getAddress(), udpInput.getPort());
                        udpServerSocket.send(udpOutput);
                        break;
                    case -1:
                        // TODO create filter to prevent bruteforce
                        logger.info("Wrong");
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        udpServerSocket.close();
    }

    /**
     * Adds knock and checks current sequence of user's knocks.
     * @param ip    user's address.
     * @param port  user's port.
     * @return      1 if sequence is correct and full.
     *              0 if sequence is correct but not full.
     *              -1 if sequence is wrong.
     */
    private synchronized int addKnock(String ip, int port) {
        String key = ip + ":" + port;

        // Get current knocks of user
        ArrayList<Integer> currUserKnocks = usersKnocksMap.get(key);
        if (currUserKnocks == null)
            currUserKnocks = new ArrayList<>();

        currUserKnocks.add(this.socket);
        usersKnocksMap.put(key, currUserKnocks);

        // Check if each current knock is in a needed sequence
        for (int i = 0; i < currUserKnocks.size(); i++) {
            if (currUserKnocks.get(i) != Integer.parseInt(this.portSequence.get(i))){
                // If we encounter first not valid knock - delete this key
                // and return 'wrong'. YOU SHALL NOT PASS!
                usersKnocksMap.remove(key);
                logger.info(key + " had wrong knock in the Map");
                return -1;
            } else if (i == (portSequence.size() - 1)) {
                // Since we haven't found wrong knocks, it's a success
                // so delete key and return success
                usersKnocksMap.remove(key);
                logger.info(key + " sent successful sequence");
                return 1;
            }
        }

        // We haven't found wrong knocks but their number was not enough
        // so continue tries
        return 0;
    }
}
